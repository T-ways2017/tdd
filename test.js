const operations = require('./operations.js');
const assert = require('assert');

it('correctly calculates the sum of 1 and 3', () => {
    assert.equal(operations.additioner(1, 3), 4);
});

it('correctly calculates the difference of 33 and 3', () => {
    assert.equal(operations.soustraire(33, 3), 30);
});

it('correctly calculates the multiplication of 3 and 2 ', () => {
    assert.equal(operations.multiplication(3, 2), 6);
});

it('correctly calculates the Division of 12 and 2 ', () => {
    assert.equal(operations.division(12, 2), 6);
});